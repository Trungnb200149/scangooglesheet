module scangooglesheet

require (
	bitbucket.org/bavusua/store_tools v0.0.0-20200302180940-b9b282fc2faa
	cloud.google.com/go v0.53.0 // indirect
	github.com/golang/protobuf v1.3.4 // indirect
	github.com/spf13/cast v1.3.1
	golang.org/x/net v0.0.0-20200301022130-244492dfa37a // indirect
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e // indirect
	golang.org/x/sys v0.0.0-20200301040627-c5d0d7b4ec88 // indirect
	google.golang.org/api v0.19.0
	google.golang.org/genproto v0.0.0-20200228133532-8c2c7df3a383 // indirect
)
