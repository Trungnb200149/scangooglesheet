# Dockerfile References: https://docs.docker.com/engine/reference/builder/
# Start from the latest golang base image
FROM golang:latest
RUN mkdir /app
ADD . /app

RUN go get -v -u github.com/golang/dep/cmd/dep
RUN dep init -v && dep ensure -v

WORKDIR /app
RUN go build -o main
CMD ["/app/main"]
