package main

import (
	"fmt"
	"scangooglesheet/gg_services"
	base_log "scangooglesheet/pkg/base-log"
	_ "scangooglesheet/pkg/cache"
	"time"
)

func main() {
	timeStart := time.Now()
	ticker := time.NewTicker(time.Second * 3)
	//myChannel(chan , 1)
	success := true
	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			if !success {
				fmt.Println("Not success now: ", time.Since(timeStart))
				break
			}
			//fmt.Println("Ticker call, current time: ", time.Since(timeStart))
			base_log.Infow(fmt.Sprintf("Time to scan, current time: %v", time.Since(timeStart)))
			success = false
			go func() {
				success = startWorking()
			}()
		}
	}
}

func startWorking() (endProcess bool) {
	defer func() {
		if re := recover(); re != nil {
			//log re, push slack?
		}
		endProcess = true
	}()
	gg_services.StartScanSheet()
	return true
}
