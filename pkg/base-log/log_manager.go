package base_log

import (
	"flag"
	"fmt"
	"go/build"
	"log"
	"os"
)

var (
	_log      *log.Logger
)

func init() {
	// set location of log file
	getLog()
}

func getLog() *log.Logger {
	if _log != nil  {
		return _log
	}
	var logpath = build.Default.GOPATH + "/src/scangooglesheet/logger/info.log"

	flag.Parse()
	file, err := os.Create(logpath)
	if err != nil {
		//panic(err)
		return nil
	}
	//defer file.Close()

	_log = log.New(file, "", log.LstdFlags|log.Lshortfile)
	_log.Println("LogFile : " + logpath)
	return _log
}

func Infow(mess string,args ...interface{}) {
	localLog := getLog()
	if localLog == nil {
		return
	}
	localLog.Println(fmt.Sprintf("{ level: Info, message: %v }", mess), args)
}

func Errorw(mess string,args ...interface{}) {
	localLog := getLog()
	if localLog == nil {
		return
	}
	localLog.Println(fmt.Sprintf("{ level: Error, message: %v }", mess), args)
}

func Warnw(mess string,args ...interface{}) {
	localLog := getLog()
	if localLog == nil {
		return
	}
	localLog.Println(fmt.Sprintf("{ level: Warn, message: %v }", mess), args)
}
