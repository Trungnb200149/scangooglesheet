package cache

var _globalCache *globalCache
type globalCache struct {
	CurrentTimeMidNightUnix int64
	CurrentScanIndexRow int64
}
func init() {
	if _globalCache == nil {
		_globalCache = &globalCache{
			CurrentTimeMidNightUnix: -1,
			CurrentScanIndexRow:     -1,
		}
	}
}

func GetGlobalCache() *globalCache {
	return _globalCache
}
