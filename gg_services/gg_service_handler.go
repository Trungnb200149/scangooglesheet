package gg_services

import (
	"bitbucket.org/bavusua/store_tools/api/model"
	"context"
	"encoding/json"
	"fmt"
	"github.com/spf13/cast"
	"golang.org/x/oauth2"
	"google.golang.org/api/option"
	"google.golang.org/api/sheets/v4"
	"log"
	"net/http"
	"os"
	independent_utils "scangooglesheet/gg_services/independent-utils"
	base_log "scangooglesheet/pkg/base-log"
	"scangooglesheet/pkg/cache"
	"time"
)

func StartScanSheet() {
	//b, err := ioutil.ReadFile("credentials.json")
	//if err != nil {
	//	log.Fatalf("Unable to read client secret file: %v", err)
	//}

	// If modifying these scopes, delete your previously saved token.json.
	config, err := independent_utils.GetConfigHardCode()
	if err != nil {
		base_log.Errorw("Unable to parse client secret file to config","error", err)
	}
	client := getClient(config)
	//defer rs.Body.Close()
	//
	srv, err := sheets.NewService(context.Background(), option.WithHTTPClient(client))
	if err != nil {
		base_log.Errorw("Unable to retrieve Sheets client","error", err)
	}

	spreadsheetId := "1UNzw44FQamygxhgXkENfMXX7zNgQLRcDfUqYGWvikGk"
	readRange := "Sheet1!A2:D"
	resp, err := srv.Spreadsheets.Values.Get(spreadsheetId, readRange).Do()
	if err != nil {
		base_log.Errorw("Unable to retrieve data from sheet","error", err)
	}

	if len(resp.Values) == 0 {
		base_log.Warnw("No data found.")
	} else {
		insertNewOrder(resp.Values)
	}
}

func getClient(config *oauth2.Config) *http.Client {
	// The file token.json stores the user's access and refresh tokens, and is
	// created automatically when the authorization flow completes for the first
	// time.
	tokFile := "token.json"
	tok, err := tokenFromFile(tokFile)
	if err != nil {
		tok = getTokenFromWeb(config)
		saveToken(tokFile, tok)
	}
	return config.Client(context.Background(), tok)
}

// Request a token from the web, then returns the retrieved token.
func getTokenFromWeb(config *oauth2.Config) *oauth2.Token {
	authURL := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
	fmt.Printf("Go to the following link in your browser then type the "+
		"authorization code: \n%v\n", authURL)

	var authCode string
	if _, err := fmt.Scan(&authCode); err != nil {
		log.Fatalf("Unable to read authorization code: %v", err)
	}

	tok, err := config.Exchange(context.TODO(), authCode)
	if err != nil {
		log.Fatalf("Unable to retrieve token from web: %v", err)
	}
	return tok
}

// Retrieves a token from a local file.
func tokenFromFile(file string) (*oauth2.Token, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	tok := &oauth2.Token{}
	err = json.NewDecoder(f).Decode(tok)
	return tok, err
}

// Saves a token to a file path.
func saveToken(path string, token *oauth2.Token) {
	fmt.Printf("Saving credential file to: %s\n", path)
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Fatalf("Unable to cache oauth token: %v", err)
	}
	defer f.Close()
	json.NewEncoder(f).Encode(token)
}

func insertNewOrder (listRow [][]interface{}) {
	err := prepareCache()
	if err != nil {
		// log
		return
	}
	var (
		firstIndex = int64(-1)
		orders = make([]*model.Order, 0)
		globalCache = cache.GetGlobalCache()
		startTime, startCache = globalCache.CurrentTimeMidNightUnix, globalCache.CurrentScanIndexRow
	)
	for idxRow, row := range listRow {
		if int64(idxRow) <=  globalCache.CurrentScanIndexRow { // today has scanned to index row in sheet
			continue
		}
		if firstIndex == -1{
			firstIndex = int64(idxRow)
			base_log.Infow(fmt.Sprintf("start from index : %v", firstIndex))
		}
		globalCache.CurrentScanIndexRow = int64(idxRow)
		orders = append(orders, &model.Order{})
		for idxCol, _ := range row {
			val := cast.ToString(row[idxCol])
			switch idxCol {
			case 0:
				// line item
			case 1:
				orders[idxRow].PhoneNumber = val
			case 2:
				orders[idxRow].InputAddress = val
			case 3:
				t, err := time.Parse(time.RFC822, val)
				if err != nil {
					fmt.Println(err)
					t = time.Now()
				}
				orders[idxRow].CreatedAt = t.Unix()
			default:
				break
			}
		}
	}
	err = syncDataOfTheLastScan(firstIndex, orders)
	if err != nil {
		globalCache.CurrentTimeMidNightUnix = startTime
		globalCache.CurrentScanIndexRow = startCache
	}
}

func prepareCache() (err error){
	globalCache := cache.GetGlobalCache()
	if globalCache.CurrentTimeMidNightUnix == -1 || globalCache.CurrentScanIndexRow == -1 {
		globalCache.CurrentTimeMidNightUnix, globalCache.CurrentScanIndexRow, err = getTimeAndIndexRowFromDatabase()
		if err != nil {
			return
		}
	}
	now := time.Now()
	midNight := time.Date(now.Year(), now.Month(), now.Day(), 0, 0 ,0, 0, time.UTC)
	if globalCache.CurrentTimeMidNightUnix < midNight.Unix() {
		globalCache.CurrentTimeMidNightUnix = midNight.Unix()
		globalCache.CurrentScanIndexRow = -1
	}

	return nil
}

func getTimeAndIndexRowFromDatabase () (currentTimeUnix, currentIndexRow int64, err error){

	return int64(0), int64(0), nil
}

func syncDataOfTheLastScan(firstIndex int64, orders []*model.Order) error {
	// get last item, sync data to that item

	// insert the rest orders
	return nil
}
